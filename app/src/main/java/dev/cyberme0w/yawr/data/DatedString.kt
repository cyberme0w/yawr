package dev.cyberme0w.yawr.data

import java.time.LocalDateTime

data class DatedString(
    val string: String,
    val datetime: LocalDateTime,
)