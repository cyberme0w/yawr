package dev.cyberme0w.yawr.data

import kotlinx.serialization.Serializable
import java.time.LocalDateTime
import java.time.ZoneOffset

enum class SunRequirement {
    // Full 6 hours of direct sunlight.
    FULL_SUN,

    // Between 3 to 6 hours of direct sunlight. Can tolerate shade, but still
    // require sunlight to grow.
    PARTIAL_SUN,

    // Between 3 to 6 hours of sunlight, but should not be exposed to intense
    // mid-day sunlight.
    PARTIAL_SHADE,

    // Less than 3 hours of direct sunlight.
    FULL_SHADE,
}

enum class Season { WINTER, SPRING, SUMMER, AUTUMN }

enum class AdditionalIdentifierType(val abbrev: String) {
    NONE("(none)"),
    VARIETY("var."),
    CULTIVAR("cv."),
    HYBRID("x.")
}

@Serializable
data class Plant(
    val id: Int,
    val datetimeAdded: Long, // Stored as UNIX Epoch timestamp

    // Naming
    var nickname: String? = null,
    var botanicalName: String? = null,
    var additionalIdentifier: String? = null,
    var additionalIdentifierAbbrev: AdditionalIdentifierType = AdditionalIdentifierType.NONE,

    // Watering/Sun Requirements
    var waterFrequencySummer: Int? = null,
    var waterFrequencyWinter: Int? = null,
    var sunRequirements: SunRequirement? = null,

    // Repotting / Dormancy Data
    var isDormant: Boolean? = null,
    var dormancyPeriod: Season? = null,

    // Descendants
    val children: ArrayList<Plant> = arrayListOf(),
    var parent: Plant? = null,
)
{
    override fun equals(other: Any?): Boolean {
        if (other is Plant) { return other.id == this.id }
        return super.equals(other)
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + datetimeAdded.hashCode()
        result = 31 * result + (nickname?.hashCode() ?: 0)
        result = 31 * result + (botanicalName?.hashCode() ?: 0)
        result = 31 * result + (additionalIdentifier?.hashCode() ?: 0)
        result = 31 * result + (additionalIdentifierAbbrev.hashCode())
        result = 31 * result + (waterFrequencySummer ?: 0)
        result = 31 * result + (waterFrequencyWinter ?: 0)
        result = 31 * result + (sunRequirements?.hashCode() ?: 0)
        result = 31 * result + (isDormant?.hashCode() ?: 0)
        result = 31 * result + (dormancyPeriod?.hashCode() ?: 0)
        result = 31 * result + children.hashCode()
        result = 31 * result + (parent?.hashCode() ?: 0)
        return result
    }
}

fun debugPlant(id: Int): Plant {
    return Plant(
        id = id,
        datetimeAdded = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC),
        nickname = "Debug Jeff",
        botanicalName = "Haworthiopsis limifolia",
        additionalIdentifierAbbrev = AdditionalIdentifierType.NONE,
        additionalIdentifier = "arcana",
        dormancyPeriod = Season.WINTER,
        isDormant = false,
        sunRequirements = SunRequirement.FULL_SUN,
        waterFrequencySummer = 14,
        waterFrequencyWinter = 10,
    )
}