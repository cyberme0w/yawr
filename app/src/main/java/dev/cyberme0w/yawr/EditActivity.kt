package dev.cyberme0w.yawr

import android.R.layout.simple_spinner_dropdown_item
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.get
import dev.cyberme0w.yawr.data.Plant
import dev.cyberme0w.yawr.databinding.ActivityEditBinding
import dev.cyberme0w.yawr.utils.StorageUtils
import kotlinx.serialization.json.Json

private const val TAG = "EditActivity"

class EditActivity : AppCompatActivity() {
    private lateinit var b: ActivityEditBinding
    private lateinit var plant: Plant

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b = ActivityEditBinding.inflate(layoutInflater)
        setContentView(b.root)

        val plantString = intent.getStringExtra("plant")
        if (plantString.isNullOrEmpty()) {
            Log.e(TAG, "plantString was null or empty")
            return
        }
        plant = Json.decodeFromString<Plant>(plantString)

        b.nickname.setText(plant.nickname, TextView.BufferType.EDITABLE)
        b.botanicalName.setText(plant.botanicalName, TextView.BufferType.EDITABLE)

        val additionalIdentifierOptions = arrayOf(
            getString(R.string.prop_ident_none),
            getString(R.string.prop_ident_cultivar),
            getString(R.string.prop_ident_variety),
            getString(R.string.prop_ident_hybrid),
        )
        val additionalIdentifierAbbrevs = arrayOf(
            "",
            getString(R.string.prop_ident_cultivar_abbrev),
            getString(R.string.prop_ident_variety_abbrev),
            getString(R.string.prop_ident_hybrid_abbrev),
        )
        assert(additionalIdentifierAbbrevs.size == additionalIdentifierOptions.size)

        b.additionalIdentifierAbbrev.adapter = ArrayAdapter(this, simple_spinner_dropdown_item, additionalIdentifierOptions)
        b.additionalIdentifierAbbrev.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                b.additionalIdentifier.isEnabled = position != 0
                b.additionalIdentifier.isActivated = position != 0
                b.additionalIdentifierLayout.prefixText = additionalIdentifierAbbrevs[position]
                if (position == 0) {
                    b.additionalIdentifier.clearFocus()
                    b.additionalIdentifier.text = null
                }
            }
        }

        b.additionalIdentifier.setText(plant.additionalIdentifier, TextView.BufferType.EDITABLE)

        b.dormancyPeriod.setText(plant.dormancyPeriod.toString(), TextView.BufferType.EDITABLE)
        b.sunRequirement.setText(plant.sunRequirements.toString(), TextView.BufferType.EDITABLE)
        b.waterFreqSummer.setText(plant.waterFrequencySummer.toString(), TextView.BufferType.EDITABLE)
        b.waterFreqWinter.setText(plant.waterFrequencyWinter.toString(), TextView.BufferType.EDITABLE)

        b.cancel.setOnClickListener { finish() }

        b.save.setOnClickListener {
            plant.nickname = b.nickname.text.toString()
            plant.botanicalName = b.botanicalName.text.toString()
            plant.additionalIdentifier = b.additionalIdentifier.text.toString()
            StorageUtils.updatePlant(this, plant)
            finish()
        }
    }
}