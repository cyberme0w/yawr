package dev.cyberme0w.yawr

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import dev.cyberme0w.yawr.adapter.PlantsAdapter
import dev.cyberme0w.yawr.data.Plant
import dev.cyberme0w.yawr.data.debugPlant
import dev.cyberme0w.yawr.databinding.ActivityMainBinding
import dev.cyberme0w.yawr.databinding.NavHeaderBinding
import dev.cyberme0w.yawr.utils.StorageUtils
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.time.LocalDateTime
import java.time.ZoneOffset

private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {
    private lateinit var b: ActivityMainBinding
    private lateinit var plants: ArrayList<Plant>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b = ActivityMainBinding.inflate(layoutInflater)
        setContentView(b.root)

        // Fetch plants from storage
        plants = StorageUtils.getPlants(applicationContext)
        // TODO: Write debugListOfPlants() that returns a list of plants with different
        //  compositions of names (e.g. no common name, but all others; or only a single botanical
        //  name)
        //plants.addAll(listOf(debugPlant(1), debugPlant(2), debugPlant(3)))

        Log.i(TAG, "onCreate: Fetched ${plants.size} plants from storage")

        // Fetch the version
        val packageInfo = packageManager.getPackageInfo(packageName, 0)
        val version = packageInfo.versionName

        // Populate nav-drawer elements
        val navHeaderBinding = NavHeaderBinding.bind(b.navView.getHeaderView(0))
        navHeaderBinding.drawerYawr.text = getString(
            R.string.app_name_with_version,
            getString(R.string.app_name),
            version
        )

        // Create Plant button
        b.fab.setOnClickListener { onFabClick() }

        // Plant Recycler
        b.plantRecycler.layoutManager = LinearLayoutManager(this)
        b.plantRecycler.adapter = PlantsAdapter(this, plants)
        //b.plantRecycler.adapter?.notifyItemRangeInserted(0, plants.size)
        b.plantRecycler.adapter?.notifyDataSetChanged()
    }

    private fun onFabClick() {
        val intent = Intent(applicationContext, EditActivity::class.java)
        intent.putExtra("plant", Json.encodeToString(Plant(
            id = plants.last().id + 1,
            datetimeAdded = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC),
        )))
        applicationContext.startActivity(intent)
    }
}
