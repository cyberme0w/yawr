package dev.cyberme0w.yawr.utils

import android.content.Context
import android.util.Log
import androidx.core.content.edit
import dev.cyberme0w.yawr.data.Plant
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

private const val prefKey = "persistence"
private const val TAG = "StorageUtils"

private const val key_plants = "KEY_PLANTS"
private const val key_last_plant_id = "KEY_LAST_PLANT_UID"

object StorageUtils {
    fun getPlants(ctx: Context): ArrayList<Plant> {
        val p = ctx.getSharedPreferences(prefKey, Context.MODE_PRIVATE)
        val fetchedStrings = p.getStringSet(key_plants, null)
        if (fetchedStrings == null) {
            Log.d(TAG, "getPlants: no plants in storage yet - returning empty list")
            return arrayListOf()
        }
        val plantList = fetchedStrings.map { plantString -> Json.decodeFromString<Plant>(plantString) }
        return plantList as ArrayList<Plant>
    }

    fun updatePlant(ctx: Context, plant: Plant) {
        val plants = getPlants(ctx)
        val indexOfPlant = plants.indexOf(plant)
        if (indexOfPlant != -1) {
            plants[indexOfPlant] = plant
        } else {
            plants.add(plant)

        }
        setPlants(ctx, plants)
    }

    fun setPlants(ctx: Context, plants: List<Plant>) {
        val p = ctx.getSharedPreferences(prefKey, Context.MODE_PRIVATE)

        val plantStringSet = plants.map { Json.encodeToString(it) }
        p.edit {
            putStringSet(key_plants, plantStringSet.toMutableSet())
        }
    }
}