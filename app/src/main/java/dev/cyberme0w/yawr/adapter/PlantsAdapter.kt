package dev.cyberme0w.yawr.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.cyberme0w.yawr.EditActivity
import dev.cyberme0w.yawr.data.AdditionalIdentifierType
import dev.cyberme0w.yawr.data.Plant
import dev.cyberme0w.yawr.databinding.ItemPlantBinding
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class PlantsAdapter(private val ctx: Context, private val plants: List<Plant>) :
    RecyclerView.Adapter<PlantsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemPlantBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount() = plants.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val plant: Plant = plants[position]
        holder.bind(plant)
        holder.itemView.setOnClickListener {
            val intent = Intent(ctx, EditActivity::class.java)
            intent.putExtra("plant", Json.encodeToString(plant))
            ctx.startActivity(intent)
        }
    }

    class ViewHolder(private val b: ItemPlantBinding) : RecyclerView.ViewHolder(b.root) {
        fun bind(plant: Plant) {
            b.nickname.text = plant.nickname
            b.botanicalName.text = plant.botanicalName
            if (plant.additionalIdentifierAbbrev == AdditionalIdentifierType.NONE) {
                b.additionalIdentifierAbbrev.text = ""
                b.additionalIdentifier.text = ""
            } else {
                b.additionalIdentifierAbbrev.text = plant.additionalIdentifierAbbrev.abbrev
                b.additionalIdentifier.text = plant.additionalIdentifier
            }
            b.daysLeft.text = "thirsty in XX day(s)"
        }
    }
}